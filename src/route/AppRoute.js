import React from 'react'
import { Routes, Route } from 'react-router'
import HomePage from '../pages/HomePage'

function AppRoute() {
  return (
    <>
    <Routes>
        <Route index path = '/' Component={HomePage}/>
    </Routes>
    </>
  )
}

export default AppRoute