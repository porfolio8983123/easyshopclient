import React from 'react'
import { Card, Container, Row } from 'react-bootstrap'
import Slider from 'react-slick'
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

function NewArrival() {

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000
      };

  return (
    <>
        <Container className='text-center' fluid = {true}>
            <div className='section-title text-center mb-5'>
                <h2>New Arrival</h2>
                <p>Some Of Our Exclusive Collection, You May Like</p>
            </div>
            <Row>
                <Slider {...settings}>
                    <div>
                        <Card className='image-box card'>
                            <img className='center' src="https://rukminim2.flixcart.com/image/612/612/xif0q/watch/m/v/o/2-brand-a-digital-watch-with-square-led-shockproof-multi-original-imags6b6vrrdfepf.jpeg?q=70" alt='imagedod'/>
                            <Card.Body>
                                <p className='product-name-on-card'>hala </p>
                                <p className='product-price-on-card'>Price : $125</p>
                            </Card.Body>
                        </Card>
                    </div>
                    <div>
                        <Card className='image-box card'>
                            <img className='center' alt='qweqwe' src="https://rukminim2.flixcart.com/image/612/612/kdakakw0/watch/4/6/x/ad1166-skmei-original-imafu89vz5tutden.jpeg?q=70"/>
                            <Card.Body>
                                <p className='product-name-on-card'>SKMEI</p>
                                <p className='product-price-on-card'>Price : $125</p>
                            </Card.Body>
                        </Card>
                    </div>
                    <div>
                    <Card className='image-box card'>
                            <img className='center' alt='ouioui' src="https://rukminim2.flixcart.com/image/612/612/k3q76a80/vehicle-pull-along/v/p/f/pull-and-go-monster-car-toy-for-kids-and-boys-spincart-original-imafmshfrweqt3hc.jpeg?q=70"/>
                            <Card.Body>
                                <p className='product-name-on-card'>spincart Pull and Go Monster</p>
                                <p className='product-price-on-card'>Price : $125</p>
                            </Card.Body>
                        </Card>
                    </div>
                    <div>
                    <Card className='image-box card'>
                            <img className='center' alt='ytrerte' src="https://rukminim2.flixcart.com/image/612/612/l4d2ljk0/combo-kit/t/a/a/face-combo-50ml-fixer-weightless-foundation-natural-2-original-imagf9saasc2g2qw.jpeg?q=70"/>
                            <Card.Body>
                                <p className='product-name-on-card'>FACES CANADA Face Combo</p>
                                <p className='product-price-on-card'>Price : $125</p>
                            </Card.Body>
                        </Card>
                    </div>
                    <div>
                    <Card className='image-box card'>
                            <img className='center' alt='yrbcdgf' src="https://rukminim2.flixcart.com/image/612/612/xif0q/brief/g/n/e/90-15-lux-cozi-original-imagggagzmkzatys.jpeg?q=70"/>
                            <Card.Body>
                                <p className='product-name-on-card'>LUX cozi</p>
                                <p className='product-price-on-card'>Price : $125</p>
                            </Card.Body>
                        </Card>
                    </div>
                    <div>
                    <Card className='image-box card'>
                            <img className='center' alt='fhgfsdfsd' src="https://rukminim2.flixcart.com/image/612/612/jy65j0w0/trunk/h/h/z/s-n107com2-one8-original-imafgghmfbn3edyh.jpeg?q=70"/>
                            <Card.Body>
                                <p className='product-name-on-card'>one8</p>
                                <p className='product-price-on-card'>Price : $125</p>
                            </Card.Body>
                        </Card>
                    </div>
                </Slider>
            </Row>
        </Container>
    </>
  )
}

export default NewArrival