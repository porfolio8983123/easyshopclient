import React from 'react'
import { Card, Col, Container, Row } from 'react-bootstrap'

function Categories() {
  return (
    <>
        <Container className='text-center' fluid = {true}>
            <div className='section-title text-center mb-5'>
                <h2>CATEGORIES</h2>
                <p>Some Of Our Exclusive Collection, You May Like</p>
            </div>
            <Row>
                <Col xl = {6} lg = {6} md = {2} sm = {12} xs = {12}>
                    <Row>
                        <Col className='p-0' xl = {3} lg = {3} md = {3} sm = {6} xs = {6}>
                            <Card className='h-100 w-100 text-center'>
                                <Card.Body>
                                    <img className='center' alt='wereyrtydgfdfg' src='https://rukminim1.flixcart.com/flap/80/80/image/22fddf3c7da4c4f4.png?q=100'/>
                                    <h5 className='category-name'>Top Offers</h5>
                                </Card.Body>
                            </Card>
                        </Col>

                        <Col className='p-0' xl = {3} lg = {3} md = {3} sm = {6} xs = {6}>
                            <Card className='h-100 w-100 text-center'>
                                <Card.Body>
                                    <img className='center' alt='sfdsghfjytrr' src='https://rukminim1.flixcart.com/flap/80/80/image/29327f40e9c4d26b.png?q=100'/>
                                    <h5 className='category-name'>Grocery</h5>
                                </Card.Body>
                            </Card>
                        </Col>

                        <Col className='p-0' xl = {3} lg = {3} md = {3} sm = {6} xs = {6}>
                            <Card className='h-100 w-100 text-center'>
                                <Card.Body>
                                    <img className='center' alt='sdfwewsfdb' src='https://rukminim1.flixcart.com/flap/80/80/image/22fddf3c7da4c4f4.png?q=100'/>
                                    <h5 className='category-name'>MOBILE</h5>
                                </Card.Body>
                            </Card>
                        </Col>

                        <Col className='p-0' xl = {3} lg = {3} md = {3} sm = {6} xs = {6}>
                            <Card className='h-100 w-100 text-center'>
                                <Card.Body>
                                    <img className='center' alt='imsdfsghage' src='https://rukminim1.flixcart.com/fk-p-flap/80/80/image/0d75b34f7d8fbcb3.png?q=100'/>
                                    <h5 className='category-name'>Fashion</h5>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Col>

                <Col xl = {6} lg = {6} md = {2} sm = {12} xs = {12}>
                    <Row>
                        <Col className='p-0' xl = {3} lg = {3} md = {3} sm = {6} xs = {6}>
                            <Card className='h-100 w-100 text-center'>
                                <Card.Body>
                                    <img className='center' alt='isdfsdmage' src='https://rukminim1.flixcart.com/flap/80/80/image/69c6589653afdb9a.png?q=100'/>
                                    <h5 className='category-name'>Electronics</h5>
                                </Card.Body>
                            </Card>
                        </Col>

                        <Col className='p-0' xl = {3} lg = {3} md = {3} sm = {6} xs = {6}>
                            <Card className='h-100 w-100 text-center'>
                                <Card.Body>
                                    <img className='center' alt='imagsfdgdhe' src='https://rukminim1.flixcart.com/flap/80/80/image/ab7e2b022a4587dd.jpg?q=100'/>
                                    <h5 className='category-name'>Home</h5>
                                </Card.Body>
                            </Card>
                        </Col>

                        <Col className='p-0' xl = {3} lg = {3} md = {3} sm = {6} xs = {6}>
                            <Card className='h-100 w-100 text-center'>
                                <Card.Body>
                                    <img className='center' alt='imaghrtrefe' src='https://rukminim1.flixcart.com/flap/80/80/image/0ff199d1bd27eb98.png?q=100'/>
                                    <h5 className='category-name'>Appliances</h5>
                                </Card.Body>
                            </Card>
                        </Col>

                        <Col className='p-0' xl = {3} lg = {3} md = {3} sm = {6} xs = {6}>
                            <Card className='h-100 w-100 text-center'>
                                <Card.Body>
                                    <img className='center' alt='sdfgdewberfw' src='https://rukminim1.flixcart.com/flap/80/80/image/dff3f7adcf3a90c6.png?q=100'/>
                                    <h5 className='category-name'>Beauty</h5>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Container>
    </>
  )
}

export default Categories