import React from 'react'
import { Container,Row,Col,Card } from 'react-bootstrap'

function Collection() {
  return (
    <>
        <Container className='text-center' fluid = {true}>
            <div className='section-title text-center mb-5'>
                <h2>Product Collection</h2>
                <p>Some Of Our Exclusive Collection, You May Like</p>
            </div>
            <Row>
                <Col className='p-0' xl = {3} lg = {3} md = {6} sm = {6} xs = {6}>
                    <Card className='image-box card w-100'>
                        <img className='center w-75' alt='sdfsdbchgdter' src="https://rukminim2.flixcart.com/image/832/832/kyrlifk0/trouser/i/v/h/38-pitfsslb779705-peter-england-original-imagax8agevwbhm3.jpeg?q=70"/>
                        <Card.Body>
                            <p className='product-name-on-card'>Men Slim Fit Black Viscose Rayon Trousers</p>
                            <p className='product-price-on-card'>Price : $125</p>
                        </Card.Body>
                    </Card>
                </Col>

                <Col className='p-0' xl = {3} lg = {3} md = {6} sm = {6} xs = {6}>
                    <Card className='image-box card w-100'>
                        <img className='center w-75' alt='swerwtetytrty' src="https://rukminim2.flixcart.com/image/612/612/kgfg2vk0/thermal/p/f/y/100-lux-cott-bu-fs-rn-tro-set-lux-cottswool-original-imafwzetysmeahbk.jpeg?q=70"/>
                        <Card.Body>
                            <p className='product-name-on-card'>LUX COTT'S WOOL Blue Full Sleeves</p>
                            <p className='product-price-on-card'>Price : $125</p>
                        </Card.Body>
                    </Card>
                </Col>

                <Col className='p-0' xl = {3} lg = {3} md = {6} sm = {6} xs = {6}>
                    <Card className='image-box card w-100'>
                        <img className='center w-75' alt='bchfjjrte' src="https://rukminim2.flixcart.com/image/612/612/xif0q/t-shirt/c/b/t/xl-grl-fs49-orng-groovy-c1-leotude-original-imagptvehgu7yjye.jpeg?q=70"/>
                        <Card.Body>
                            <p className='product-name-on-card'>Realme C21 (Cross Black, 64 GB)</p>
                            <p className='product-price-on-card'>Price : $125</p>
                        </Card.Body>
                    </Card>
                </Col>

                <Col className='p-0' xl = {3} lg = {3} md = {6} sm = {6} xs = {6}>
                    <Card className='image-box card w-100'>
                        <img className='center w-75' alt='sdfwwerbdb' src="https://rukminim2.flixcart.com/image/612/612/xif0q/shirt/v/l/y/-original-imagsy6qnxgkzjnz.jpeg?q=70"/>
                        <Card.Body>
                            <p className='product-name-on-card'>Men Slim Fit Solid Formal Shirt</p>
                            <p className='product-price-on-card'>Price : $125</p>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    </>
  )
}

export default Collection